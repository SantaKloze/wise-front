# wise

# PAGES


    2 vues
    - Home (recherche fullscreen [div] + affichage de tous les résultats de recherche [hide si fullscreen])
        - Search
        - Fullscreen
        - Cards formation
        - Filtres
        - Header
        - Derières offres (si le temps)
    - Formation > accès via id dans url (/formation/:id) > requète sur l'api à chaque chargement d'une view via cette route pour récupérer toutes les infos
        - Plus d'informations


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
