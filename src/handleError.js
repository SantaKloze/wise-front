/* eslint-disable import/no-cycle */
/* eslint-disable no-else-return */
/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
import store from './store';
import router from './router';
import Vue from 'vue'

function parseError(code) {
  let error = "";
  let isError = true

  console.log(code.message)
  if (code.response) code.message = code.response.message;
  if (code === 401) {
    Vue.prototype.$session.destroy();
    Vue.prototype.$store.commit('isConnected', false)
    error = 'Vous n\'êtes plus connecté, veuillez vous reconnecter.';
  }
  // CAS FORMULAIRE
  // OBLIGE D'UTILISER code.message SI VOUUS FAITES UN THROW D'ERREUR. LE "code" ETANT ALORS L'ERREUR ELLE MÊME

  else if (code === 422 || code === 403) {
    error = 'Veuillez remplir tout le formulaire pour continuer.';
  }
  else if (code === 500) {
    error = 'Erreur interne. Veuillez réessayer dans un cours instant.';
  }
  else if (code === 'not_connected') {
    error = 'Veuillez vous connecter pour accéder à la plateforme d\'administration.';
  }
  else if (code.message === 'no_pass') {
    error = 'Veuillez entrer votre ancien mot de passe.';
  }
  else if (code.message === 'no_new_pass') {
    error = 'Veuillez entrer votre nouveau mot de passe.';
  }
  else if (code.message === 'ERR_FORBIDDEN') {
    error = 'Action interdite.';
  }
  else if (code.message === 'ERR_NEW_PASSWORD_IS_LIKE_OLD') {
    error = 'Votre nouveau mot de passe ne peut pas être le même que l\'ancien';
  }
  else if (code.message === 'ERR_PASSWORD_DOESNT_MATCH') {
    error = 'Votre mot de passe actuel n\'est pas bon.';
  }
  else if (code.message === 'no_conf_new_pass') {
    error = 'Veuillez entrer la confirmation de votre nouveau mot de passe.';
  }
  else if (code.message === 'password_not_match') {
    error = 'Le nouveau mot de passe ne corresponds pas à la confirmation.';
  }
  else if (code.message === 'miss_pass') {
    error = 'Veuillez remplir tous les champs avant de continuer.';
  }
  else if (code.message === 'error_while_connect') {
    error = 'Il y a eu une erreur pendant la connexion, veuillez vérifier vos identifiants ou réessayer plus tard';
  }
  else if (code.message === 'Network Error') {
    error = 'La requète n\'a pas abouti, veuillez réessayer dans quelques instants.';
  }
  if (!isError) {
    if (!error) error = "Votre action a bien été effectuée."
    Vue.prototype.$toasted.show(error, { 
      theme: "toasted-alert",
      icon: "error",
      position: "bottom-left",
      duration : 2000
    });
  }
  else {
    if (!error) error = "Une erreur est survenue."
    Vue.prototype.$toasted.show(error, { 
      theme: "toasted-error",
      icon: "warning",
      position: "bottom-left",
      duration : 5000
    });
  }
}

// eslint-disable-next-line import/prefer-default-export
export { parseError };
