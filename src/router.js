import Vue from 'vue'
import Router from 'vue-router'
import Offers from './components/Params/Offers.vue'
import Account from './components/Params/Account.vue'
import Management from './components/Params/Management.vue'
import { parseError } from './handleError';
import store from './store';

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
      {
        path: '/',
        name: 'home',
        component: () => import(/* webpackChunkName: "about" */ './views/Home.vue')
      },
      {
        path: '/formation/:id',
        name: 'fullForm',
        component: () => import(/* webpackChunkName: "about" */ './views/Formation.vue')
      },
      {
        path: '/admin',
        name: 'admin',
        component: () => import(/* webpackChunkName: "about" */ './views/Admin.vue'),
        children: [
            {
                path: "",
                name: "offers",
                component: Offers
            },
            {
                path: "Account",
                name: "account",
                component: Account
            },
            {
                path: "Management",
                name: "management",
                component: Management
            }
        ]
    },
  ]
});

router.beforeEach(async (to, from, next) => {
  try {
    console.log(to)
    if (to.path === '/admin/' && !store.state.isConnected || to.path === '/admin' && !store.state.isConnected || to.path === '/admin/Account' && !store.state.isConnected || to.path === '/admin/Account/' && !store.state.isConnected || to.path === '/admin/Management' && !store.state.isConnected === '/admin/Management/' ) {
      store.commit('triggerError', 'not_connected');
      router.push('/');
    }
    next();

    } catch (error) {
      parseError(error);
    }
});

export default router;