import Vue from 'vue'
import Vuex from 'vuex'
import { parseError } from './handleError'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // check if first co
    isFirstCo: true,

    // get search val
    searchVal: '',

    // get search val
    isConnected: true,
  },
  mutations: {
    isFirstCo(state, isFirstCo) {
      state.isFirstCo = isFirstCo;
    },
    searchVal(state, searchVal) {
      state.searchVal = searchVal
    },
    isConnected(state, isConnected) {
      state.isConnected = isConnected
    },
    triggerError(state, code) {
      parseError(code);
    },
  },
  actions: {

  }
})
